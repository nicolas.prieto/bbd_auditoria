
-- Tabla para escoger casos de prueba de calificaciones
-------------------------------------------------------------------------
DROP TABLE IF EXISTS proceso.control_calif;
-------------------------------------------------------------------------
CREATE TABLE proceso.control_calif STORED AS PARQUET AS WITH pa AS (
  SELECT
    num_id,
    modelo_evaluado,
    g_comportamiento,
    g_sin_exp,
    g_acierta,
    grupo_de_riesgo,
    fte_grupo_de_riesgo,
    ROW_NUMBER() OVER(
      PARTITION BY g_acierta,
      modelo_evaluado
      ORDER BY
        g_acierta
    ) AS rn
  FROM {tabla_resultados}
)
SELECT  
  pa.num_id,
  pa.modelo_evaluado,
  buro.score_acierta_gf,
  pa.g_comportamiento,
  pa.g_sin_exp,
  pa.g_acierta,
  pa.grupo_de_riesgo,
  pa.fte_grupo_de_riesgo
FROM {tabla_buro} AS buro
INNER JOIN pa ON pa.num_id = CAST(buro.num_doc AS BIGINT)
WHERE
  buro.ingestion_year = {buro_ing[year]}
  AND buro.ingestion_month = {buro_ing[month]}
  AND buro.ingestion_day = {buro_ing[day]}
  AND pa.rn BETWEEN 1
  AND {nxcaso};
  -------------------------------------------------------------------------